# requirement
docker
```
Install Docker CE
```
lando
```
Download the latest .deb from https://github.com/lando/lando/releases
```
```
sudo dpkg -i lando-XXX.deb
```

# create new folder project
```
mkdir my-first-drupal8-app
```
```
cd my-first-drupal8-app/
```

# initialize a drupal8 recipe using the latest drupal 8 version
```
lando init \
  --source remote \
  --remote-url https://www.drupal.org/download-latest/tar.gz \
  --remote-options="--strip-components 1" \
  --recipe drupal8 \
  --webroot . \
  --name my-first-drupal8-app
```

# start lando up
```
lando start
```

# default database connection information
```
database: drupal8
username: drupal8
password: drupal8
host: database
port: 3306
```

# direct link of drupal
```
http://my-first-drupal8-app.lndo.site:8000
```

# lando commands
Runs composer commands
```
lando composer
```
Exports database from a service into a file
```
lando db-export [file]
```
Imports a dump file into database service
```
lando db-import <file>
```
Runs drupal console commands
```
lando drupal
```
Runs drush commands
```
lando drush             
```
Drops into a MySQL shell on a database service
```
lando mysql
```
Runs php commands
```
lando php
```
